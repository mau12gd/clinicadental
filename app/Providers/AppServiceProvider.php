<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;//
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*este metodo nos ayudara hacer las migraciones a la 
        base de datos y le colecara por defaul la longitud 191 a los string*/
        Schema::defaultStringLength(191);
    }
}
