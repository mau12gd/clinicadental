<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Citas extends Model
{
    //se declara la tabla
    protected $table = 'citas';
    //se declaran los campos de la tabla
   protected $fillable =['fecha','hora','cliente_id','servicio_id'];
    public function Citas()
	{
		return $this->hasMany('App\servicio::class');
		return $this->hasMany('App\cliente::class');
	} 
}
