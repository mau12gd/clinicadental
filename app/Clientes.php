<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    //se declara la tabla
     protected $table = 'cliente';
     //se declaran los campos de la tabla
    protected $fillable =['nombre','apellido','direccion','telefono'];
    
}
