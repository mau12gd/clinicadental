<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
     //se declara la tabla
     protected $table = 'servicio';
     //se declaran los campos de la tabla
    protected $fillable =['nombre','descripcion','precio'];
}
