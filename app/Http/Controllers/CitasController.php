<?php

namespace App\Http\Controllers;
use App\Citas;
use App\Clientes;
use App\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CitasController extends Controller
{
    public function index( Request $request)
    {
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $citas=DB::table('citas.citas as c')
            ->join('cliente as t','c.cliente_id','=','t.id')
            ->join('servicio as s','c.servicio_id','=','s.id')
            ->select('c.id','c.cliente_id','c.fecha','c.hora','t.nombre as cliente','t.apellido','t.telefono','t.direccion','s.nombre as servicio', 's.precio')
            ->where('c.fecha','LIKE','%'.$sql.'%')
            ->orWhere('c.fecha','LIKE','%'.$sql.'%')
            ->orderBy('c.fecha','ASC')
            ->paginate(10);

            $clientes=DB::table('cliente')
            ->select('id','nombre','apellido','direccion','telefono');
            $servicio=DB::table('servicio')
            ->select('id','nombre','descripcion','precio');
            return view('citas.index',["citas"=>$citas,"clientes"=>$clientes,"servicio"=>$servicio,"buscarTexto"=>$sql]);
            //return view('producto.index'); 
        }
    
    }
    public function create()
    {
         //que nos debuelva todas las categorias
         $clientes = Clientes::all();
         $servicio = Servicio::all();
         return view('citas.create', compact('servicio','clientes'));
    }
    public function store(Request $request)
    {
        $datosProducto=request()->all();
        $datosProducto=request()->except('_token');
        Citas::insert($datosProducto);
        //return response()->json($datosProducto);
        return redirect('home/citas');  
    }
    public function show(Citas $citas)
    {
        //
    }
    public function edit($id)
    {
        $cita=Citas::findOrFail($id);  
        return view('citas.edit', compact('cita'));
    }

    public function update(Request $request, $id)
    {
        $datosCita=request()->except(['_token','_method']);
      Citas::where('id','=',$id)->update($datosCita);
      $cita =Citas::findOrFail($id);
      return view('citas.edit', compact('cita'));
    }
    public function destroy($id)
    {
          $cita=Citas::findOrFail($id);
        Citas::destroy($id);  
         return redirect('home/citas');
    }
}
