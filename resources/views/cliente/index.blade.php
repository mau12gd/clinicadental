

@extends('layouts.app')       
@section('content')  
<div class="comumn col-12 col-sm-12">
      <ul class="ul"  >
          
          <li class="li"><a class="active" href="{{asset('home')}}">inicio</a></li>
          <li class="li"><a class="a" href="{{asset('home/servicio')}}">servicios</a></li>
          <li class="li"><a  class="a" href="{{asset('home/cliente')}}">Datos de cliente</a></li>
          <li class="li"><a  class="a" href="{{asset('home/citas/create')}}">Agendar Cita</a></li>
          <li class="li"><a  class="a"href="{{('citas')}}">Ver Citas</a></li>
        </ul>
    </div> 
 <a href="{{asset(url('home/cliente/create'))}}"class="botton3  text-decoration-none" >Crear cliente</button></a>
            <table class="resp">
            <h1 class="titl">Listado de Clientes</h1>
            
                <thead >
                    <tr>  
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">direccion</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($clientes as $cliente)
                    <tr>   
                        <td class="td-c">{{$cliente->nombre}}</td>
                        <td class="td-c">{{$cliente->apellido}}</td> 
                        <td class="td-c">{{$cliente->direccion}}</td>
                        <td class="td-c">{{$cliente->telefono}}</td>
                        <td class="td-c">
                            <a href="{{url('/home/cliente/'.$cliente->id.'/edit') }}"> 
                            <button class="botton" type="submit" id="id" name="id" >Modificar</button></a>
                           
                            <form action="{{ url('home/cliente/'.$cliente->id) }}"   method="POST">
                               @method('DELETE')
                               @csrf
                            <button class="botton2" type="submit" onclick="return confirm('¿Desea Eliminar el Dato?')">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>    
        </div>
            </table>
@endsection
