@extends('layouts.app')
@section('content')
<div class="comumn col-12 col-sm-12">
      <ul class="ul"  >
          <li class="li"><a class="active" href="{{asset('home')}}">inicio</a></li>
          <li class="li"><a class="active" href="{{asset('home')}}">inicio</a></li>
          <li class="li"><a class="a" href="{{asset('home/servicio')}}">servicios</a></li>
          <li class="li"><a  class="a" href="{{asset('home/cliente')}}">Datos de cliente</a></li>
          <li class="li"><a  class="a" href="{{asset('home/citas/create')}}">Agendar Cita</a></li>
          <li class="li"><a  class="a"href="{{('home/citas')}}">Ver Citas</a></li>
        </ul>
    </div>
<div class="form col-12" >
<h1 class="nomCliente" >Modificar servicio</h1> 
    <form action="{{url('home/servicio/'.$servicio->id) }}" method="post" enctype="multipart/form-data"> 
            <!-- token de seguridad -->
            {{ csrf_field() }}
            <!-- Tipo de solicitud | _method -->
             {{method_field('PATCH')}}
             @include ('/servicio.form', ['modo'=>'editar'])
        </form>
    </div>         

@endsection
  