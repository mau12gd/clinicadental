@extends('layouts.app')       
@section('content') 
<div class="comumn col-12 col-sm-12">
      <ul class="ul"  >
          <li class="li"><a class="active" href="{{asset('home')}}">inicio</a></li>
          <li class="li"><a class="a" href="{{asset('home/servicio')}}">servicios</a></li>
          <li class="li"><a  class="a" href="{{asset('home/cliente')}}">Datos de cliente</a></li>
          <li class="li"><a  class="a" href="{{asset('home/citas/create')}}">Agendar Cita</a></li>
          <li class="li"><a  class="a"href="{{asset('home/citas')}}">Ver Citas</a></li>
        </ul>
    </div>   
 <a href="{{asset(url('home/servicio/create'))}}"class="botton3  text-decoration-none" >Crear servicio</button></a>
            <table class="resp">
            <h1 class="titl">Listado de Servicio</h1>
            
                <thead >
                    <tr>  
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($servicio as $servis)
                    <tr>   
                        <td class="td-s">{{$servis->nombre}}</td>
                        <td class="td-s">{{$servis->descripcion}}</td> 
                        <td class="td-s">Q{{$servis->precio}}</td>
                        <td class="td-s">
                             <a href="{{url('/home/servicio/'.$servis->id.'/edit') }}"> 
                            <button class="botton" type="submit" id="id" name="id" >Modificar</button></a>
                           
                            <form action="{{ url('home/servicio/'.$servis->id) }}"   method="POST">
                               @method('DELETE')
                               @csrf
                            <button class="botton2" type="submit" onclick="return confirm('¿Desea Eliminar el Dato?')">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>    
        </div>
            </table>
@endsection
