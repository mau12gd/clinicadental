
@extends('layouts.app')
     
@section('content') 
<div class="comumn col-12 col-sm-12">
      <ul class="ul"  >
          <li class="li"><a class="active" href="{{asset('home')}}">inicio</a></li>
          <li class="li"><a class="a" href="{{asset('home/servicio')}}">servicios</a></li>
          <li class="li"><a  class="a" href="{{asset('home/cliente')}}">Datos de cliente</a></li>
          <li class="li"><a  class="a" href="{{asset('home/citas/create')}}">Agendar Cita</a></li>
          <li class="li"><a  class="a"href="{{('citas')}}">Ver Citas</a></li>
        </ul>
    </div>
     <script src="{{asset('JS/form.js')}}"> </script>
         <div class="form col-12" >   
        <form action="{{url('home/citas')}}" class="form-horizontal" method="post" name="formulario" onSubmit="return valida(this);">
            {{ csrf_field() }} <!--/*nos imprime una llave de acceso*/-->
          <h1 class="nomCliente" >Informacion de Citas</h1> 

        <label for="nombre" >Fecha</label><br>
        <input type="date" id="fecha"name="fecha" class="caja" placeholder="aa/mm/dd" required><br>

         <label for="hora" >Hora</label>
         <br><input type="time" id="hora" name="hora" class="caja" placeholder="00:00"required><br>
         
        <label for="servicio">Servicio</label><br>
        <div >
        <select class="form-control" name="servicio_id" id="servicio_id" required>      
           <option value="">--Seleccione la servicio--</option>
           @foreach ($servicio as $servis)
           <option value="{{$servis['id']}}">{{$servis['nombre']}}</option>    
           @endforeach
        </select>
        <label for="cliente">Cliente</label><br>
        <div>
        <select class="form-control" name="cliente_id" id="cliente_id" required>      
           <option value="">--Seleccione la cliente--</option>
           @foreach ($clientes as $cliente)
           <option value="{{$cliente['id']}}">{{$cliente['nombre']}}</option>    
           @endforeach
        </select>
            
    <input type="submit" class="boton" value="crear"> 
    <a href="{{ url('home/citas') }}" class="boton">Regresar</a>
</div>
        </form>
        
  
@endsection
