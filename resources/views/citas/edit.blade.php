@extends('layouts.app')

@section('content')


<div class="form col-12" >
<h1 class="nomCliente" >Modificar Cita</h1> 
    <form action="{{url('home/citas/'.$cita->id) }}" method="post" enctype="multipart/form-data"> 
            <!-- token de seguridad -->
            {{ csrf_field() }}
            <!-- Tipo de solicitud | _method -->
             {{method_field('PATCH')}}
             @include ('/citas.form', ['modo'=>'editar'])
        </form>
    </div>         

@endsection
  