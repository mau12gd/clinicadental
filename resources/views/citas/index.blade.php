
@extends('layouts.app')       
@section('content') 
<div class="comumn col-12 col-sm-12">
      <ul class="ul"  >
          <li class="li"><a class="active" href="{{asset('home')}}">inicio</a></li>
          <li class="li"><a class="a" href="{{asset('home/servicio')}}">servicios</a></li>
          <li class="li"><a  class="a" href="{{asset('home/cliente')}}">Datos de cliente</a></li>
          <li class="li"><a  class="a" href="{{asset('home/citas/create')}}">Agendar Cita</a></li>
          <li class="li"><a  class="a"href="{{asset('home/citas')}}">Ver Citas</a></li>
        </ul>
    </div> 
 <a href="{{asset(url('home/citas/create'))}}"class="botton3  text-decoration-none" >Crear cita</button></a>
            <table class="resp">
            <h1 class="titl">Listado de Citas</h1>
            
                <thead >
                    <tr>  
                        <th scope="col">Cliente</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">fecha</th>
                         <th scope="col">hora</th>
                          <th scope="col">servicio</th>
                           <th scope="col">precio</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($citas as $cita)
                    <tr>   
                        <td class="td-v">{{$cita->cliente}}  {{$cita->apellido}}</td>
                        <td class="td-v">{{$cita->telefono}}</td> 
                        <td class="td-v">{{$cita->direccion}}</td>
                        <td class="td-v">{{$cita->fecha}}</td>
                          <td class="td-v">{{$cita->hora}}</td>
                           <td class="td-v">{{$cita->servicio}}</td>
                           <td class="td-v">{{$cita->precio}}</td>
                        
                        <td class="td-c">
                            <a href="{{url('home/citas/'.$cita->id.'/edit') }}"> 
                            <button class="botton" type="submit" id="id" name="id" >Modificar</button></a>
                           
                            <form action="{{ url('home/citas/'.$cita->id) }}"   method="POST">
                               @method('DELETE')
                               @csrf
                            <button class="botton2" type="submit" onclick="return confirm('¿Desea Eliminar el Dato?')">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>    
        </div>
            </table>
@endsection
